#include <glib.h>
#include "meego/meego-imcontext-dbus.h"
#include "meego/qt-translate.h"

#include "context.h"
#include "mimclient.h"

gboolean redirect_keys = FALSE;

gboolean meego_imcontext_client_activation_lost_event(MeegoIMContextDbusObj *obj)
{
	g_debug("Got %s", __func__);
	return TRUE;
}

gboolean meego_imcontext_client_im_initiated_hide(MeegoIMContextDbusObj *obj)
{
	g_debug("Got %s", __func__);
	return TRUE;
}

gboolean meego_imcontext_client_commit_string(MeegoIMContextDbusObj *obj,
                                              const char *string,
                                              int replace_start, int replace_length, int cursor_pos)
{
	return context_commit(contexts_focused_icid(), string,
	                      replace_start, replace_length, cursor_pos);
}

gboolean meego_imcontext_client_update_preedit(MeegoIMContextDbusObj *obj, const char *string,
                                               int preedit_format_count, const MeegoImPreeditFormat preedit_formats[],
                                               int replace_start, int replace_length, int cursor_pos)
{
	return context_update_preedit(contexts_focused_icid(),
	                              string, preedit_format_count, preedit_formats,
	                              replace_start, replace_length, cursor_pos);
}

gboolean meego_imcontext_client_key_event(MeegoIMContextDbusObj *obj, int type, int key, int modifiers, char *text,
        gboolean auto_repeat, int count)
{
	MeegoImKeyEvent e;
	meego_im_key_event_decode(&e, type, key, modifiers, text);
	return context_send_key(contexts_focused_icid(), &e);
}

gboolean meego_imcontext_client_update_input_method_area(MeegoIMContextDbusObj *obj, GPtrArray *data)
{
	g_debug("Got %s", __func__);
	return TRUE;
}

gboolean meego_imcontext_client_set_global_correction_enabled(MeegoIMContextDbusObj *obj, gboolean correction)
{
	g_debug("Got %s", __func__);
	return TRUE;
}

gboolean meego_imcontext_client_copy(MeegoIMContextDbusObj *obj)
{
	g_debug("Got %s", __func__);
	return TRUE;
}

gboolean meego_imcontext_client_paste(MeegoIMContextDbusObj *obj)
{
	g_debug("Got %s", __func__);
	return TRUE;
}

gboolean meego_imcontext_client_set_redirect_keys(MeegoIMContextDbusObj *obj, gboolean enabled)
{
	redirect_keys = enabled;
	return TRUE;
}

gboolean meego_imcontext_client_preedit_rectangle(MeegoIMContextDbusObj *obj, GValueArray **rect, gboolean *valid)
{
	g_debug("Got %s", __func__);
	return TRUE;
}
