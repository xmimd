CFLAGS?=-g -Wall -O0
LDFLAGS?=-Wl,-O1 -Wl,--as-needed

XMIMD_TARGET:=xmimd
XMIMD_OBJS:=main.o ximserver.o mimclient.o context.o
XMIMD_PKGCONFIG:=glib-2.0 gobject-2.0 dbus-glib-1 x11
XMIMD_CFLAGS:=$(shell pkg-config --cflags $(XMIMD_PKGCONFIG)) -std=gnu99 -DG_LOG_DOMAIN=\"xmim\"
XMIMD_LDFLAGS:=
XMIMD_LIBS:=$(shell pkg-config --libs $(XMIMD_PKGCONFIG))

XMIMTEST_TARGET:=test
XMIMTEST_OBJS:=test.o
XMIMTEST_PKGCONFIG:=x11
XMIMTEST_CFLAGS:=$(shell pkg-config --cflags $(XMIMTEST_PKGCONFIG)) -std=gnu99
XMIMTEST_LDFLAGS:=
XMIMTEST_LIBS:=$(shell pkg-config --libs $(XMIMTEST_PKGCONFIG))

all: $(XMIMD_TARGET) $(XMIMTEST_TARGET)

$(XMIMD_TARGET): $(XMIMD_OBJS) IMdkit/libIMdkit.a meego/libmeego-im-common.a
	$(CC) $(XMIMD_LDFLAGS) $(LDFLAGS) -o $@ $+ $(XMIMD_LIBS) $(LIBS)

$(XMIMTEST_TARGET): $(XMIMTEST_OBJS)
	$(CC) $(XMIMTEST_LDFLAGS) $(LDFLAGS) -o $@ $+ $(XMIMTEST_LIBS) $(LIBS)

$(XMIMD_OBJS): %.o: %.c
	$(CC) $(XMIMD_CFLAGS) $(CFLAGS) -o $@ -c $<

$(XMIMTEST_OBJS): %.o: %.c
	$(CC) $(XMIMTEST_CFLAGS) $(CFLAGS) -o $@ -c $<

IMdkit/libIMdkit.a:
	$(MAKE) -C IMdkit all

meego/libmeego-im-common.a:
	$(MAKE) -C meego all

install: $(XMIMD_TARGET)
	install -d $(DESTDIR)/usr/bin
	install -m 0755 $(XMIMD_TARGET) $(DESTDIR)/usr/bin

uninstall:
	rm -f $(DESTDIR)/usr/bin/$(XMIMD_TARGET)

clean:
	rm -f *.o $(XMIMD_TARGET) $(XMIMTEST_TARGET)
	$(MAKE) -C IMdkit clean
	$(MAKE) -C meego clean

.PHONY: all clean
