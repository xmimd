#ifndef XMIM_H
#define XMIM_H

#include <glib.h>
#include <X11/Xlib.h>

extern Display *x_dpy;
extern Window x_win;
extern XIM x_im;
extern XIC x_ic;

extern char * opt_display;
extern gint64 opt_xephyr;

extern gboolean opt_translucent;

#endif
