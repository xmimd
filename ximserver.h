#ifndef XIMSERVER_H
#define XIMSERVER_H

#include "IMdkit/IMdkit.h"
#include "IMdkit/Xi18n.h"
#include "meego/qt-translate.h"

void xims_open();
void xims_close();

void xims_commit(guint imid, guint icid, guint keysym, const char *text);
void xims_forward_event(IMForwardEventStruct *call_data);
void xims_call_callback(XPointer data);

void xims_insert_event(guint imid, guint icid, const XEvent *xev);

#endif
