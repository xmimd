#ifndef CONTEXT_H
#define CONTEXT_H

#include <glib.h>
#include <X11/Xlib.h>
#include "IMdkit/IMdkit.h"
#include "IMdkit/Xi18n.h"
#include "meego/meego-im-defs.h"

void contexts_init();
void contexts_destroy();
guint contexts_focused_icid();

/* The following functions get the icid from call_data */
gboolean context_create(IMChangeICStruct *call_data);
gboolean context_destroy(IMChangeICStruct *call_data);

gboolean context_set_values(IMChangeICStruct *call_data);
gboolean context_get_values(IMChangeICStruct *call_data);

gboolean context_set_focus(IMChangeFocusStruct *call_data);
gboolean context_unset_focus(IMChangeFocusStruct *call_data);

gboolean context_forward_event(IMForwardEventStruct *call_data);

gboolean context_reset(IMResetICStruct *call_data);

gboolean context_commit(guint icid, const char *string,
                        int replace_start, int replace_length, int cursor_pos);
gboolean context_update_preedit(guint icid, const char *string,
                                int preedit_format_count, const MeegoImPreeditFormat *preedit_formats,
                                int replace_start, int replace_length, int cursor_pos);
gboolean context_send_key(guint icid, const MeegoImKeyEvent* e);

#endif
