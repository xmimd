/*
 * Copyright (C) 2010, Intel Corporation.
 *
 * Author: Raymond Liu <raymond.liu@intel.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef _QT_TRANSLATE_H
#define _QT_TRANSLATE_H

#include <glib.h>
#include "meego-im-defs.h"

G_BEGIN_DECLS

void meego_im_key_event_decode(MeegoImKeyEvent *e, int type, int key, int modifiers, char *text);
void meego_im_key_event_encode(const MeegoImKeyEvent *e, int *type, int *key, int *modifiers, char **text);

G_END_DECLS

#endif //_QT_GTK_TRANSLATE_H
