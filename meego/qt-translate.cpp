/*
 * Copyright (C) 2010, Intel Corporation.
 *
 * Author: Raymond Liu <raymond.liu@intel.com>
 * 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "debug.h"

#include <QEvent>
#include <QKeyEvent>

#include "qt-keysym-map.h"

#include "qt-translate.h"

enum {
	ShiftModifier = 1 << 0,
	ControlModifier = 1 << 2,
	AltModifier = 1 << 3
};
#define SHIFT_MASK 1 << 0)

void meego_im_key_event_decode(MeegoImKeyEvent *e, int type, int key, int modifiers, char *text)
{
	g_warn_if_fail(type == QEvent::KeyPress || type == QEvent::KeyRelease);
	e->release = type == QEvent::KeyRelease;

	e->state = 0;
	if (modifiers & Qt::ShiftModifier)
		e->state |= ShiftModifier;
	if (modifiers & Qt::ControlModifier)
		e->state |= ControlModifier;
	if (modifiers & Qt::AltModifier)
		e->state |= AltModifier;

	e->keysym = QtKeyToXKeySym(key);

	if (text && strlen(text) > 0) {
		e->text = text;
	}
}

void meego_im_key_event_encode(const MeegoImKeyEvent *e, int *type, int *key, int *modifiers, char **text)
{
	*type = e->release ? QEvent::KeyRelease : QEvent::KeyPress;
	*key = XKeySymToQTKey(e->keysym);
	*modifiers = Qt::NoModifier;
	if (e->state & ShiftModifier)
		*modifiers |= Qt::ShiftModifier;
	if (e->state & ControlModifier)
		*modifiers |= Qt::ControlModifier;
	if (e->state & AltModifier)
		*modifiers |= Qt::AltModifier;
	*text = e->text && strlen(e->text) > 0 ? e->text : NULL;
}
