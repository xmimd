/* * This file partially from the meego-im-framework *
 *
 * Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
 * All rights reserved.
 * Contact: Nokia Corporation (directui@nokia.com)
 *
 * If you have questions regarding the use of this file, please contact
 * Nokia at directui@nokia.com.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 * and appearing in the file LICENSE.LGPL included in the packaging
 * of this file.
 */

#ifndef MEEGO_IM_DEFS_H
#define MEEGO_IM_DEFS_H

#include <glib.h>

enum MeegoImOrientationAngle {
    MeegoImAngle0   =   0,
    MeegoImAngle90  =  90,
    MeegoImAngle180 = 180,
    MeegoImAngle270 = 270
};

/*!
 * This enum contains possible values for orientation of windows in the application.
 *
 * \sa OrientationAngle
 */
enum MeegoImOrientation {
    MeegoImPortrait, //!< equal to either M::Angle90 or M::Angle270 orientation angles
    MeegoImLandscape //!< equal to either M::Angle0 or M::Angle180 orientation angles
};

//! Content type for text entries. Used at least with MTextEdit
enum MeegoImTextContentType {
    //! all characters allowed
    MeegoImFreeTextContentType,

    //! only integer numbers allowed
    MeegoImNumberContentType,

    //! allows numbers and certain other characters used in phone numbers
    MeegoImPhoneNumberContentType,

    //! allows only characters permitted in email address
    MeegoImEmailContentType,

    //! allows only character permitted in URL address
    MeegoImUrlContentType,

    //! allows content with user defined format
    MeegoImCustomContentType
};

enum MeegoImInputMethodMode {
    //! Normal mode allows to use preedit and error correction
    MeegoImInputMethodModeNormal,

    //! Virtual keyboard sends QKeyEvent for every key press or release
    MeegoImInputMethodModeDirect,

    //! Used with proxy widget
    MeegoImInputMethodModeProxy
};

typedef enum _MeegoImPreeditFace {
    MeegoImPreeditDefault,
    MeegoImPreeditNoCandidates,
    MeegoImPreeditKeyPress           //! Used for displaying the hwkbd key just pressed
} MeegoImPreeditFace;

typedef struct _MeegoImPreeditFormat {
	int start;
	int length;
	MeegoImPreeditFace face;
} MeegoImPreeditFormat;

typedef struct _MeegoImKeyEvent
{
	gboolean release;
	unsigned int state;
	unsigned int keysym;
	char *text;
} MeegoImKeyEvent;

static const char * const MeegoImInfoTranslucent = "maliit-translucent-input-method";

#endif
