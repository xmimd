/* Generated by dbus-binding-tool; do not edit! */


#ifndef __dbus_glib_marshal_meego_imcontext_dbus_MARSHAL_H__
#define __dbus_glib_marshal_meego_imcontext_dbus_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

#ifdef G_ENABLE_DEBUG
#define g_marshal_value_peek_boolean(v)  g_value_get_boolean (v)
#define g_marshal_value_peek_char(v)     g_value_get_char (v)
#define g_marshal_value_peek_uchar(v)    g_value_get_uchar (v)
#define g_marshal_value_peek_int(v)      g_value_get_int (v)
#define g_marshal_value_peek_uint(v)     g_value_get_uint (v)
#define g_marshal_value_peek_long(v)     g_value_get_long (v)
#define g_marshal_value_peek_ulong(v)    g_value_get_ulong (v)
#define g_marshal_value_peek_int64(v)    g_value_get_int64 (v)
#define g_marshal_value_peek_uint64(v)   g_value_get_uint64 (v)
#define g_marshal_value_peek_enum(v)     g_value_get_enum (v)
#define g_marshal_value_peek_flags(v)    g_value_get_flags (v)
#define g_marshal_value_peek_float(v)    g_value_get_float (v)
#define g_marshal_value_peek_double(v)   g_value_get_double (v)
#define g_marshal_value_peek_string(v)   (char*) g_value_get_string (v)
#define g_marshal_value_peek_param(v)    g_value_get_param (v)
#define g_marshal_value_peek_boxed(v)    g_value_get_boxed (v)
#define g_marshal_value_peek_pointer(v)  g_value_get_pointer (v)
#define g_marshal_value_peek_object(v)   g_value_get_object (v)
#define g_marshal_value_peek_variant(v)  g_value_get_variant (v)
#else /* !G_ENABLE_DEBUG */
/* WARNING: This code accesses GValues directly, which is UNSUPPORTED API.
 *          Do not access GValues directly in your code. Instead, use the
 *          g_value_get_*() functions
 */
#define g_marshal_value_peek_boolean(v)  (v)->data[0].v_int
#define g_marshal_value_peek_char(v)     (v)->data[0].v_int
#define g_marshal_value_peek_uchar(v)    (v)->data[0].v_uint
#define g_marshal_value_peek_int(v)      (v)->data[0].v_int
#define g_marshal_value_peek_uint(v)     (v)->data[0].v_uint
#define g_marshal_value_peek_long(v)     (v)->data[0].v_long
#define g_marshal_value_peek_ulong(v)    (v)->data[0].v_ulong
#define g_marshal_value_peek_int64(v)    (v)->data[0].v_int64
#define g_marshal_value_peek_uint64(v)   (v)->data[0].v_uint64
#define g_marshal_value_peek_enum(v)     (v)->data[0].v_long
#define g_marshal_value_peek_flags(v)    (v)->data[0].v_ulong
#define g_marshal_value_peek_float(v)    (v)->data[0].v_float
#define g_marshal_value_peek_double(v)   (v)->data[0].v_double
#define g_marshal_value_peek_string(v)   (v)->data[0].v_pointer
#define g_marshal_value_peek_param(v)    (v)->data[0].v_pointer
#define g_marshal_value_peek_boxed(v)    (v)->data[0].v_pointer
#define g_marshal_value_peek_pointer(v)  (v)->data[0].v_pointer
#define g_marshal_value_peek_object(v)   (v)->data[0].v_pointer
#define g_marshal_value_peek_variant(v)  (v)->data[0].v_pointer
#endif /* !G_ENABLE_DEBUG */


/* BOOLEAN:INT,INT,INT,STRING,BOOLEAN,INT,UCHAR,POINTER */
extern void dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__INT_INT_INT_STRING_BOOLEAN_INT_UCHAR_POINTER (GClosure     *closure,
                                                                                                          GValue       *return_value,
                                                                                                          guint         n_param_values,
                                                                                                          const GValue *param_values,
                                                                                                          gpointer      invocation_hint,
                                                                                                          gpointer      marshal_data);
void
dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__INT_INT_INT_STRING_BOOLEAN_INT_UCHAR_POINTER (GClosure     *closure,
                                                                                              GValue       *return_value G_GNUC_UNUSED,
                                                                                              guint         n_param_values,
                                                                                              const GValue *param_values,
                                                                                              gpointer      invocation_hint G_GNUC_UNUSED,
                                                                                              gpointer      marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__INT_INT_INT_STRING_BOOLEAN_INT_UCHAR_POINTER) (gpointer     data1,
                                                                                          gint         arg_1,
                                                                                          gint         arg_2,
                                                                                          gint         arg_3,
                                                                                          gpointer     arg_4,
                                                                                          gboolean     arg_5,
                                                                                          gint         arg_6,
                                                                                          guchar       arg_7,
                                                                                          gpointer     arg_8,
                                                                                          gpointer     data2);
  register GMarshalFunc_BOOLEAN__INT_INT_INT_STRING_BOOLEAN_INT_UCHAR_POINTER callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;
  gboolean v_return;

  g_return_if_fail (return_value != NULL);
  g_return_if_fail (n_param_values == 9);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_BOOLEAN__INT_INT_INT_STRING_BOOLEAN_INT_UCHAR_POINTER) (marshal_data ? marshal_data : cc->callback);

  v_return = callback (data1,
                       g_marshal_value_peek_int (param_values + 1),
                       g_marshal_value_peek_int (param_values + 2),
                       g_marshal_value_peek_int (param_values + 3),
                       g_marshal_value_peek_string (param_values + 4),
                       g_marshal_value_peek_boolean (param_values + 5),
                       g_marshal_value_peek_int (param_values + 6),
                       g_marshal_value_peek_uchar (param_values + 7),
                       g_marshal_value_peek_pointer (param_values + 8),
                       data2);

  g_value_set_boolean (return_value, v_return);
}

/* BOOLEAN:INT,INT,POINTER */
extern void dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__INT_INT_POINTER (GClosure     *closure,
                                                                             GValue       *return_value,
                                                                             guint         n_param_values,
                                                                             const GValue *param_values,
                                                                             gpointer      invocation_hint,
                                                                             gpointer      marshal_data);
void
dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__INT_INT_POINTER (GClosure     *closure,
                                                                 GValue       *return_value G_GNUC_UNUSED,
                                                                 guint         n_param_values,
                                                                 const GValue *param_values,
                                                                 gpointer      invocation_hint G_GNUC_UNUSED,
                                                                 gpointer      marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__INT_INT_POINTER) (gpointer     data1,
                                                             gint         arg_1,
                                                             gint         arg_2,
                                                             gpointer     arg_3,
                                                             gpointer     data2);
  register GMarshalFunc_BOOLEAN__INT_INT_POINTER callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;
  gboolean v_return;

  g_return_if_fail (return_value != NULL);
  g_return_if_fail (n_param_values == 4);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_BOOLEAN__INT_INT_POINTER) (marshal_data ? marshal_data : cc->callback);

  v_return = callback (data1,
                       g_marshal_value_peek_int (param_values + 1),
                       g_marshal_value_peek_int (param_values + 2),
                       g_marshal_value_peek_pointer (param_values + 3),
                       data2);

  g_value_set_boolean (return_value, v_return);
}

/* BOOLEAN:POINTER */
extern void dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__POINTER (GClosure     *closure,
                                                                     GValue       *return_value,
                                                                     guint         n_param_values,
                                                                     const GValue *param_values,
                                                                     gpointer      invocation_hint,
                                                                     gpointer      marshal_data);
void
dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__POINTER (GClosure     *closure,
                                                         GValue       *return_value G_GNUC_UNUSED,
                                                         guint         n_param_values,
                                                         const GValue *param_values,
                                                         gpointer      invocation_hint G_GNUC_UNUSED,
                                                         gpointer      marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__POINTER) (gpointer     data1,
                                                     gpointer     arg_1,
                                                     gpointer     data2);
  register GMarshalFunc_BOOLEAN__POINTER callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;
  gboolean v_return;

  g_return_if_fail (return_value != NULL);
  g_return_if_fail (n_param_values == 2);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_BOOLEAN__POINTER) (marshal_data ? marshal_data : cc->callback);

  v_return = callback (data1,
                       g_marshal_value_peek_pointer (param_values + 1),
                       data2);

  g_value_set_boolean (return_value, v_return);
}

/* BOOLEAN:STRING,BOXED,INT,INT,INT,POINTER */
extern void dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__STRING_BOXED_INT_INT_INT_POINTER (GClosure     *closure,
                                                                                              GValue       *return_value,
                                                                                              guint         n_param_values,
                                                                                              const GValue *param_values,
                                                                                              gpointer      invocation_hint,
                                                                                              gpointer      marshal_data);
void
dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__STRING_BOXED_INT_INT_INT_POINTER (GClosure     *closure,
                                                                                  GValue       *return_value G_GNUC_UNUSED,
                                                                                  guint         n_param_values,
                                                                                  const GValue *param_values,
                                                                                  gpointer      invocation_hint G_GNUC_UNUSED,
                                                                                  gpointer      marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__STRING_BOXED_INT_INT_INT_POINTER) (gpointer     data1,
                                                                              gpointer     arg_1,
                                                                              gpointer     arg_2,
                                                                              gint         arg_3,
                                                                              gint         arg_4,
                                                                              gint         arg_5,
                                                                              gpointer     arg_6,
                                                                              gpointer     data2);
  register GMarshalFunc_BOOLEAN__STRING_BOXED_INT_INT_INT_POINTER callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;
  gboolean v_return;

  g_return_if_fail (return_value != NULL);
  g_return_if_fail (n_param_values == 7);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_BOOLEAN__STRING_BOXED_INT_INT_INT_POINTER) (marshal_data ? marshal_data : cc->callback);

  v_return = callback (data1,
                       g_marshal_value_peek_string (param_values + 1),
                       g_marshal_value_peek_boxed (param_values + 2),
                       g_marshal_value_peek_int (param_values + 3),
                       g_marshal_value_peek_int (param_values + 4),
                       g_marshal_value_peek_int (param_values + 5),
                       g_marshal_value_peek_pointer (param_values + 6),
                       data2);

  g_value_set_boolean (return_value, v_return);
}

/* BOOLEAN:BOXED,POINTER */
extern void dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__BOXED_POINTER (GClosure     *closure,
                                                                           GValue       *return_value,
                                                                           guint         n_param_values,
                                                                           const GValue *param_values,
                                                                           gpointer      invocation_hint,
                                                                           gpointer      marshal_data);
void
dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__BOXED_POINTER (GClosure     *closure,
                                                               GValue       *return_value G_GNUC_UNUSED,
                                                               guint         n_param_values,
                                                               const GValue *param_values,
                                                               gpointer      invocation_hint G_GNUC_UNUSED,
                                                               gpointer      marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__BOXED_POINTER) (gpointer     data1,
                                                           gpointer     arg_1,
                                                           gpointer     arg_2,
                                                           gpointer     data2);
  register GMarshalFunc_BOOLEAN__BOXED_POINTER callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;
  gboolean v_return;

  g_return_if_fail (return_value != NULL);
  g_return_if_fail (n_param_values == 3);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_BOOLEAN__BOXED_POINTER) (marshal_data ? marshal_data : cc->callback);

  v_return = callback (data1,
                       g_marshal_value_peek_boxed (param_values + 1),
                       g_marshal_value_peek_pointer (param_values + 2),
                       data2);

  g_value_set_boolean (return_value, v_return);
}

/* BOOLEAN:POINTER,POINTER,POINTER */
extern void dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__POINTER_POINTER_POINTER (GClosure     *closure,
                                                                                     GValue       *return_value,
                                                                                     guint         n_param_values,
                                                                                     const GValue *param_values,
                                                                                     gpointer      invocation_hint,
                                                                                     gpointer      marshal_data);
void
dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__POINTER_POINTER_POINTER (GClosure     *closure,
                                                                         GValue       *return_value G_GNUC_UNUSED,
                                                                         guint         n_param_values,
                                                                         const GValue *param_values,
                                                                         gpointer      invocation_hint G_GNUC_UNUSED,
                                                                         gpointer      marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__POINTER_POINTER_POINTER) (gpointer     data1,
                                                                     gpointer     arg_1,
                                                                     gpointer     arg_2,
                                                                     gpointer     arg_3,
                                                                     gpointer     data2);
  register GMarshalFunc_BOOLEAN__POINTER_POINTER_POINTER callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;
  gboolean v_return;

  g_return_if_fail (return_value != NULL);
  g_return_if_fail (n_param_values == 4);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_BOOLEAN__POINTER_POINTER_POINTER) (marshal_data ? marshal_data : cc->callback);

  v_return = callback (data1,
                       g_marshal_value_peek_pointer (param_values + 1),
                       g_marshal_value_peek_pointer (param_values + 2),
                       g_marshal_value_peek_pointer (param_values + 3),
                       data2);

  g_value_set_boolean (return_value, v_return);
}

/* BOOLEAN:STRING,INT,INT,INT,POINTER */
extern void dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__STRING_INT_INT_INT_POINTER (GClosure     *closure,
                                                                                        GValue       *return_value,
                                                                                        guint         n_param_values,
                                                                                        const GValue *param_values,
                                                                                        gpointer      invocation_hint,
                                                                                        gpointer      marshal_data);
void
dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__STRING_INT_INT_INT_POINTER (GClosure     *closure,
                                                                            GValue       *return_value G_GNUC_UNUSED,
                                                                            guint         n_param_values,
                                                                            const GValue *param_values,
                                                                            gpointer      invocation_hint G_GNUC_UNUSED,
                                                                            gpointer      marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__STRING_INT_INT_INT_POINTER) (gpointer     data1,
                                                                        gpointer     arg_1,
                                                                        gint         arg_2,
                                                                        gint         arg_3,
                                                                        gint         arg_4,
                                                                        gpointer     arg_5,
                                                                        gpointer     data2);
  register GMarshalFunc_BOOLEAN__STRING_INT_INT_INT_POINTER callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;
  gboolean v_return;

  g_return_if_fail (return_value != NULL);
  g_return_if_fail (n_param_values == 6);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_BOOLEAN__STRING_INT_INT_INT_POINTER) (marshal_data ? marshal_data : cc->callback);

  v_return = callback (data1,
                       g_marshal_value_peek_string (param_values + 1),
                       g_marshal_value_peek_int (param_values + 2),
                       g_marshal_value_peek_int (param_values + 3),
                       g_marshal_value_peek_int (param_values + 4),
                       g_marshal_value_peek_pointer (param_values + 5),
                       data2);

  g_value_set_boolean (return_value, v_return);
}

/* BOOLEAN:BOOLEAN,POINTER */
extern void dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__BOOLEAN_POINTER (GClosure     *closure,
                                                                             GValue       *return_value,
                                                                             guint         n_param_values,
                                                                             const GValue *param_values,
                                                                             gpointer      invocation_hint,
                                                                             gpointer      marshal_data);
void
dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__BOOLEAN_POINTER (GClosure     *closure,
                                                                 GValue       *return_value G_GNUC_UNUSED,
                                                                 guint         n_param_values,
                                                                 const GValue *param_values,
                                                                 gpointer      invocation_hint G_GNUC_UNUSED,
                                                                 gpointer      marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__BOOLEAN_POINTER) (gpointer     data1,
                                                             gboolean     arg_1,
                                                             gpointer     arg_2,
                                                             gpointer     data2);
  register GMarshalFunc_BOOLEAN__BOOLEAN_POINTER callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;
  gboolean v_return;

  g_return_if_fail (return_value != NULL);
  g_return_if_fail (n_param_values == 3);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_BOOLEAN__BOOLEAN_POINTER) (marshal_data ? marshal_data : cc->callback);

  v_return = callback (data1,
                       g_marshal_value_peek_boolean (param_values + 1),
                       g_marshal_value_peek_pointer (param_values + 2),
                       data2);

  g_value_set_boolean (return_value, v_return);
}

G_END_DECLS

#endif /* __dbus_glib_marshal_meego_imcontext_dbus_MARSHAL_H__ */

#include <dbus/dbus-glib.h>
static const DBusGMethodInfo dbus_glib_meego_imcontext_dbus_methods[] = {
  { (GCallback) meego_imcontext_dbus_activation_lost_event, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__POINTER, 0 },
  { (GCallback) meego_imcontext_dbus_im_initiated_hide, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__POINTER, 59 },
  { (GCallback) meego_imcontext_dbus_commit_string, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__STRING_INT_INT_INT_POINTER, 114 },
  { (GCallback) meego_imcontext_dbus_update_preedit, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__STRING_BOXED_INT_INT_INT_POINTER, 226 },
  { (GCallback) meego_imcontext_dbus_key_event, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__INT_INT_INT_STRING_BOOLEAN_INT_UCHAR_POINTER, 359 },
  { (GCallback) meego_imcontext_dbus_update_input_method_area, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__BOXED_POINTER, 488 },
  { (GCallback) meego_imcontext_dbus_set_global_correction_enabled, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__BOOLEAN_POINTER, 559 },
  { (GCallback) meego_imcontext_dbus_copy, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__POINTER, 637 },
  { (GCallback) meego_imcontext_dbus_paste, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__POINTER, 681 },
  { (GCallback) meego_imcontext_dbus_set_redirect_keys, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__BOOLEAN_POINTER, 726 },
  { (GCallback) meego_imcontext_dbus_preedit_rectangle, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__POINTER_POINTER_POINTER, 793 },
  { (GCallback) meego_imcontext_dbus_set_detectable_auto_repeat, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__BOOLEAN_POINTER, 881 },
  { (GCallback) meego_imcontext_dbus_set_selection, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__INT_INT_POINTER, 956 },
  { (GCallback) meego_imcontext_dbus_selection, dbus_glib_marshal_meego_imcontext_dbus_BOOLEAN__POINTER_POINTER_POINTER, 1029 },
};

const DBusGObjectInfo dbus_glib_meego_imcontext_dbus_object_info = {  1,
  dbus_glib_meego_imcontext_dbus_methods,
  14,
"com.meego.inputmethod.inputcontext1\0activationLostEvent\0S\0\0com.meego.inputmethod.inputcontext1\0imInitiatedHide\0S\0\0com.meego.inputmethod.inputcontext1\0commitString\0S\0string\0I\0s\0replaceStart\0I\0i\0replaceLength\0I\0i\0cursorPos\0I\0i\0\0com.meego.inputmethod.inputcontext1\0updatePreedit\0S\0string\0I\0s\0formatList\0I\0a(iii)\0replaceStart\0I\0i\0replaceLength\0I\0i\0cursorPos\0I\0i\0\0com.meego.inputmethod.inputcontext1\0keyEvent\0S\0type\0I\0i\0key\0I\0i\0modifiers\0I\0i\0text\0I\0s\0autoRepeat\0I\0b\0count\0I\0i\0requestType\0I\0y\0\0com.meego.inputmethod.inputcontext1\0updateInputMethodArea\0S\0data\0I\0ay\0\0com.meego.inputmethod.inputcontext1\0setGlobalCorrectionEnabled\0S\0enabled\0I\0b\0\0com.meego.inputmethod.inputcontext1\0copy\0S\0\0com.meego.inputmethod.inputcontext1\0paste\0S\0\0com.meego.inputmethod.inputcontext1\0setRedirectKeys\0S\0enabled\0I\0b\0\0com.meego.inputmethod.inputcontext1\0preeditRectangle\0S\0valid\0O\0F\0N\0b\0rect\0O\0F\0N\0(iiii)\0\0com.meego.inputmethod.inputcontext1\0setDetectableAutoRepeat\0S\0enabled\0I\0b\0\0com.meego.inputmethod.inputcontext1\0setSelection\0S\0start\0I\0i\0length\0I\0i\0\0com.meego.inputmethod.inputcontext1\0selection\0S\0valid\0O\0F\0N\0b\0selectionText\0O\0F\0N\0s\0\0\0",
"\0",
"\0"
};

